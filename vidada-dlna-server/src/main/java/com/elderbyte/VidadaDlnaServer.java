package com.elderbyte;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.SocketException;
import java.net.UnknownHostException;

@SpringBootApplication
public class VidadaDlnaServer {

    private static final Logger log = LoggerFactory.getLogger(VidadaDlnaServer.class);

    /**
     * Main method, used to run the application.
     */
    public static void main(String[] args) throws UnknownHostException, SocketException {
        SpringApplication app = new SpringApplication(VidadaDlnaServer.class);

        // Check if the selected profile has been set as argument.
        // if not the development profile will be added
        Environment env = app.run(args).getEnvironment();

        /*
        NetworkUtil.getAddress().ifPresent(address -> {
            log.info("Access URLs:\n----------------------------------------------------------\n\t" +
                "HTTPS: \thttp://"+address.getHostAddress()+":" + env.getProperty("server.port")  + "\n\t" +
                "\n----------------------------------------------------------"
            );
        });*/
    }

}
