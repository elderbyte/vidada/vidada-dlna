package com.elderbyte.vidada.integrations.vidada;

import com.elderbyte.vidada.client.auth.OAuthGrantRequest;
import com.elderbyte.vidada.client.auth.OAuthResponse;
import com.elderbyte.vidada.client.auth.VidadaAuthClient;
import com.elderbyte.warden.spring.local.auth.LocalAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class VidadaSecurityIntegration {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VidadaAuthClient authClient;

    @Autowired
    private VidadaClientSettings clientSettings;

    private String token;


    public void authorize(){
        try {

            OAuthGrantRequest request = new OAuthGrantRequest(
                "password",
                clientSettings.getVidadaClientUser(),
                clientSettings.getVidadaClientKey()
            );

            OAuthResponse response = authClient.getAccessToken(request);
            token = response.access_token;

        }catch (Exception e){
            log.warn("Failed to authorize on vidada-server as user " + clientSettings.getVidadaClientUser() + "!", e);
        }
    }


    public Optional<String> getToken() {
        return Optional.ofNullable(token);
    }
}
