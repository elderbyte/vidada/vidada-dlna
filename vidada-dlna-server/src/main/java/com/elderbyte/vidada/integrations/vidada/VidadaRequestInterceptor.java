package com.elderbyte.vidada.integrations.vidada;

import com.elderbyte.warden.spring.local.feign.interceptors.BearerTokenRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VidadaRequestInterceptor extends BearerTokenRequestInterceptor {

    @Autowired
    private VidadaSecurityIntegration securityIntegration;

    @Override
    public String getBearerToken() {
        return securityIntegration.getToken().orElse(null);
    }
}
