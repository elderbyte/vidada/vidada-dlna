package com.elderbyte.vidada.integrations.vidada;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class VidadaClientSettings {

    // Client Integration Properties

    @Value("${vidada.client.user}")
    private String vidadaClientUser;

    @Value("${vidada.client.key}")
    private String vidadaClientKey;



    public String getVidadaClientUser() {
        return vidadaClientUser;
    }

    public String getVidadaClientKey() {
        return vidadaClientKey;
    }
}
