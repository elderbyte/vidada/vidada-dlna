package com.elderbyte.vidada.integrations.vidada;

import com.elderbyte.commons.data.contiunation.ContinuableListing;
import com.elderbyte.commons.data.contiunation.ContinuationToken;
import com.elderbyte.spring.cloud.bootstrap.support.ContinuablePageSupport;
import com.elderbyte.spring.cloud.bootstrap.support.PageableContinuationToken;
import com.elderbyte.vidada.browser.MediaBrowserService;
import com.elderbyte.vidada.client.documents.BrowseNodeDto;
import com.elderbyte.vidada.client.documents.VidadaMediaBrowseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class VidadaClientMediaBrowserService implements MediaBrowserService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VidadaMediaBrowseClient vidadaClient;


    public ContinuableListing<BrowseNodeDto> listNodesContinuable(String vpath, String queryStr, String tagExpression, Sort sort, ContinuationToken token) {
        try {
            return vidadaClient.getBrowseNodes(vpath, queryStr, tagExpression, true, sort, token.getToken());
        }catch (Exception e){
            log.warn("Failed to load medias from vidada server! vpath: " + vpath, e);
            return ContinuableListing.empty();
        }
    }


    @Override
    public Page<BrowseNodeDto> listNodes(String vpath, String queryStr, String tagExpression, Pageable pageable) {

        var token = PageableContinuationToken.buildToken(pageable); // Hack for now
        var sort = pageable.getSort();

        var continuableList = listNodesContinuable(vpath, queryStr, tagExpression, sort, token);

        return convertToPage(continuableList, pageable);
    }

    private static <T> Page<T> convertToPage(ContinuableListing<T> continuableListing, Pageable pageable){
        var total = continuableListing.getTotal() != null ? continuableListing.getTotal() : continuableListing.getContent().size();
        return new PageImpl<>(continuableListing.getContent(), pageable, total);
    }
}
