package com.elderbyte.vidada;

import com.elderbyte.vidada.dlna.DlnaService;
import com.elderbyte.vidada.integrations.vidada.VidadaSecurityIntegration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class StartupComposerService implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DlnaService dlnaService;

    @Autowired
    private VidadaSecurityIntegration securityIntegration;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info("VidadaDlna is ready, starting dlna integration ...");

        securityIntegration.authorize();

        dlnaService.startDlnaServerAsync();
    }
}
