package com.elderbyte.vidada;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Provides a typed access to the application settings
 * defined in /resources/config/application.yml
 */
@Component
public class VidadaDlnaSettings {

    // DLNA Server Properties

    @Value("${vidada.dlna.server.name}")
    private String serverName;

    @Value("${vidada.dlna.server.port}")
    private int serverDlnaPort;

    @Value("${vidada.dlna.server.multicast.port}")
    private int serverDlnaMulticastPort;

    public String getServerName(){
        return serverName;
    }


    public int getServerDlnaPort() {
        return serverDlnaPort;
    }

    public int getServerDlnaMulticastPort() {
        return serverDlnaMulticastPort;
    }
}
