package com.elderbyte.vidada.experiments;

import com.elderbyte.vidada.client.preview.PreviewDto;
import com.elderbyte.vidada.client.preview.PreviewState;
import com.elderbyte.vidada.dlna.AbstractMediaContentDirectory;
import com.elderbyte.vidada.dlna.cling.DidlObjectBuilder;
import com.elderbyte.vidada.dlna.cling.MovieDataDto;
import org.fourthline.cling.support.model.BrowseResult;
import org.fourthline.cling.support.model.DIDLObject;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ExperimentalMediaContentDirectory extends AbstractMediaContentDirectory {

    public ExperimentalMediaContentDirectory(){

    }

    @Override
    protected BrowseResult handleBrowseRequest(String parentId, String search, Pageable pageable) throws Exception {

        List<DIDLObject> nodes = new ArrayList<>();

        nodes.add(
            buildPreview(parentId,
                "apple.png",
                "http://elderbyte.com/movMI.mp4",
                "http://brandmark.io/logo-rank/random/apple.png",
                "image/png")
        );

        nodes.add(
            buildPreview(parentId,
                "friends.jpg",
                "http://elderbyte.com/movMI.mp4",
                "http://www.gotosurvive.com/wp-content/uploads/2014/02/friends-having-fun-picture1.jpg",
                "image/jpeg")
        );


        nodes.add(
            buildPreview(parentId,
                "160.jpg",
                "http://elderbyte.com/testMI.mp4",
                "https://t3.ftcdn.net/jpg/00/88/17/68/160_F_88176809_hptielLPfPAvgsQvdRkE99iDIZarQj4x.jpg",
                "image/jpeg")
        );

        nodes.add(
            buildPreview(parentId,
                "aws.jpg",
                "https://elderbyte.com/test2MI.mp4",
                "https://images-na.ssl-images-amazon.com/images/I/41JezieGjOL._AC_UL320_SR320,320_.jpg",
                "image/jpeg")
        );

        return buildBrowseResult(nodes, nodes.size());
    }

    private DIDLObject buildPreview(
        String parentId,
        String id,
        String streamUrl,
        String previewUrl,
        String mimeType){

        MovieDataDto movie = new MovieDataDto();
        movie.title = "Test " + id;
        movie.mimeType = "video/mp4";
        movie.streamUrl = streamUrl;
        movie.size = 0;
        movie.duration = "00:55:43";

        PreviewDto preview = new PreviewDto(
            UUID.randomUUID(),
            previewUrl,
            PreviewState.Ready,
            0,
            mimeType
        );

        return DidlObjectBuilder.buildMovie(
            parentId,
            id,
            movie,
            preview,
            new ArrayList<>()
        );
    }

}
