package com.elderbyte.vidada.dlna;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/dlna")
public class DlnaResource {


    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
   private DlnaService dlnaService;


    @RequestMapping(method = GET, value = "/status")
    public DlnaStatusDto getStatus(){
        return new DlnaStatusDto(dlnaService.isDlnaServerRunning());
    }


    @RequestMapping(method = PUT, value = "/status")
    public DlnaStatusDto setStatus(@RequestBody DlnaStatusDto status){

        log.info("Setting DLNA Status to: " + status);

        if(status.enabled){
            // The DLNA service should be enabled
            dlnaService.startDlnaServerAsync();
        }else{
            // The DLNA service should be disabled
            dlnaService.stopDlnaServer();
        }
        return getStatus();
    }

    @RequestMapping(method = POST, value = "/server")
    public DlnaStatusDto createDLNAServer(){
        log.info("Starting DLNA Server");
        dlnaService.startDlnaServerAsync();
        return getStatus();
    }

    @RequestMapping(method = DELETE, value = "/server")
    public DlnaStatusDto removeDLNAServer(){
        log.info("Stopping DLNA Server");
        dlnaService.stopDlnaServer();
        return getStatus();
    }




}
