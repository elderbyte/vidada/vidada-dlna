package com.elderbyte.vidada.dlna;

import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.meta.*;
import org.fourthline.cling.model.types.DeviceType;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;
import org.fourthline.cling.support.connectionmanager.ConnectionManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DlnaMediaServiceBuilder {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private String name;
    private String description;


    public static DlnaMediaServiceBuilder create(String name){
        return new DlnaMediaServiceBuilder().name(name);
    }

    public DlnaMediaServiceBuilder name(String name){
        this.name = name;
        return this;
    }

    public DlnaMediaServiceBuilder description(String description){
        this.description = description;
        return this;
    }


    public LocalDevice build()
        throws ValidationException, LocalServiceBindingException, IOException {

        DeviceIdentity identity =
            new DeviceIdentity(
                UDN.uniqueSystemIdentifier(name)
            );

        DeviceType type =
            new UDADeviceType("MediaServer", 1);

        DeviceDetails details =
            new DeviceDetails(
                name,
                new ManufacturerDetails("ElderByte"),
                new ModelDetails(
                    name + "2018",
                    description,
                    "v1"
                )
            );

        /*
        Icon icon =
            new Icon(
                "image/png", 48, 48, 8,
                getClass().getResource("icon.png")
            );
        */

        return new LocalDevice(
            identity,
            type,
            details,
            new LocalService[] {
                // buildLocalService(ExperimentalMediaContentDirectory.class)
                buildLocalService(DlnaMediaContentDirectory.class),
                buildLocalService(ConnectionManagerService.class)
            });
    }

    private static  <T> LocalService<T> buildLocalService(Class<T> clazz){
        LocalService<T> connectionManagerService = new AnnotationLocalServiceBinder().read(clazz);
        connectionManagerService.setManager(new DefaultServiceManager<>(
                connectionManagerService,
                clazz
            )
        );
        return connectionManagerService;
    }


}
