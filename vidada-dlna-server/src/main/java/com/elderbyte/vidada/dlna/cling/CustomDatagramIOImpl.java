package com.elderbyte.vidada.dlna.cling;

import org.fourthline.cling.transport.Router;
import org.fourthline.cling.transport.impl.DatagramIOConfigurationImpl;
import org.fourthline.cling.transport.impl.DatagramIOImpl;
import org.fourthline.cling.transport.spi.DatagramProcessor;
import org.fourthline.cling.transport.spi.InitializationException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.util.logging.Logger;

public class CustomDatagramIOImpl extends DatagramIOImpl {

    private static Logger log = Logger.getLogger(CustomDatagramIOImpl.class.getName());

    private final int datagramPort;


    public CustomDatagramIOImpl(DatagramIOConfigurationImpl configuration, int datagramPort) {
        super(configuration);
        this.datagramPort = datagramPort;
    }

    @Override
    synchronized public void init(InetAddress bindAddress, Router router, DatagramProcessor datagramProcessor) throws InitializationException {
        this.router = router;
        this.datagramProcessor = datagramProcessor;
        try {
            log.info("Creating bound multicast socket (for datagram input/output) on: " + bindAddress + " @ port " + this.datagramPort);
            localAddress = new InetSocketAddress(bindAddress, this.datagramPort);
            socket = new MulticastSocket(localAddress);
            socket.setTimeToLive(configuration.getTimeToLive());
            socket.setReceiveBufferSize(262144); // Keep a backlog of incoming datagrams if we are not fast enough
        } catch (Exception ex) {
            throw new InitializationException("Could not initialize " + getClass().getSimpleName() + ": " + ex);
        }
    }

}
