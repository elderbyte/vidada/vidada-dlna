package com.elderbyte.vidada.dlna.cling;

import com.elderbyte.commons.exceptions.ArgumentNullException;
import com.elderbyte.vidada.DurationUtil;
import com.elderbyte.vidada.client.documents.BrowseNodeDto;
import com.elderbyte.vidada.client.documents.blobs.DocumentBlobDto;
import com.elderbyte.vidada.client.preview.PreviewDto;
import org.fourthline.cling.support.model.DIDLObject;
import org.fourthline.cling.support.model.Res;
import org.fourthline.cling.support.model.container.MovieGenre;
import org.fourthline.cling.support.model.item.Movie;
import org.seamless.util.MimeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class DidlObjectBuilder {

    private static final Logger log = LoggerFactory.getLogger(DidlObjectBuilder.class);


    public static Optional<DIDLObject> buildDlnaItem(BrowseNodeDto node, String parentId){
        if(node.hasChildren){
            return Optional.of(
                DidlObjectBuilder.buildMovieGenre(parentId, node.id, node.title)
            );
        }else{
            return DidlObjectBuilder.buildMovie(parentId, node);
        }
    }

    public static Optional<DIDLObject> buildMovie(String parentId, BrowseNodeDto node){

        if(parentId == null) throw new ArgumentNullException("parentId");
        if(node == null) throw new ArgumentNullException("node");


        if(node.blob != null){
            DocumentBlobDto blob = node.blob;

            String duration = blob.metadata != null ? DurationUtil.formatDuration(blob.metadata.duration) : "";
            long bitrate = blob.metadata != null ? (long)blob.metadata.bitrate : 0;
            Collection<String> tags = blob.metadata != null ? blob.metadata.tags : new ArrayList<>();

            MovieDataDto movie = new MovieDataDto(
                node.title,
                blob.streamUrl,
                blob.mimeType,
                blob.size,
                duration, bitrate);

            DIDLObject didlMovie = buildMovie(
                parentId, node.id,
                movie,
                node.preview,
                tags
            );

            return Optional.of(didlMovie);
        }else{
            return Optional.empty();
        }
    }


    public static DIDLObject buildMovieGenre(String parentId, String genreId, String title){
        return new MovieGenre(
            genreId,
            parentId,
            title,
            "Vidada", 0
        );
    }


    public static DIDLObject buildMovie(
        String parentId, String movieId, MovieDataDto movie,
        PreviewDto preview, Collection<String> genres
    ){

        if(parentId == null) throw new ArgumentNullException("parentId");
        if(movieId == null) throw new ArgumentNullException("movieId");
        if(movie == null) throw new ArgumentNullException("movie");

        List<Res> resources = new ArrayList<>();


        // Add the actual movie resource
        if(movie.streamUrl != null){

            MimeType mime = MimeType.valueOf(movie.mimeType);

            resources.add(
                new Res(
                    mime,
                    movie.size,
                    movie.duration,
                    movie.bitrate,
                    movie.streamUrl)
            );
        }

        // Add thumbnail data
        DIDLObject.Property.UPNP.ALBUM_ART_URI artUri = null;
        if(preview != null && preview.resourceUrl != null){
            String imageUrl = preview.resourceUrl;
            Res thumbResource = new Res(preview.mimeType, null, null, null, imageUrl);
            thumbResource.setResolution(160,160);
            resources.add(thumbResource);
            try {
                artUri = new DIDLObject.Property.UPNP.ALBUM_ART_URI(new URI(imageUrl)) ;
            } catch (URISyntaxException e) {
                artUri = null;
                log.error("Failed to create ALBUM_ART_URI", e);
            }
        }

        Movie didlMovie = new Movie(movieId, parentId, movie.title, "Vidada",
            resources.toArray(new Res[resources.size()])
        );
        didlMovie.addProperty(artUri);

        if(genres != null){
            didlMovie.setGenres(genres.toArray(new String[genres.size()]));
        }
        return didlMovie;
    }

}
