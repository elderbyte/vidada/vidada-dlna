package com.elderbyte.vidada.dlna.cling;

import com.elderbyte.commons.exceptions.ArgumentNullException;
import com.elderbyte.vidada.dlna.RemoteDeviceAuthorizer;
import com.sun.net.httpserver.HttpExchange;
import org.fourthline.cling.UpnpServiceConfiguration;
import org.fourthline.cling.model.message.IncomingDatagramMessage;
import org.fourthline.cling.protocol.ProtocolFactory;
import org.fourthline.cling.transport.RouterImpl;
import org.fourthline.cling.transport.impl.HttpExchangeUpnpStream;
import org.fourthline.cling.transport.spi.UpnpStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class CustomUpnpRouter extends RouterImpl {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static RemoteDeviceAuthorizer deviceAuthorizer; // Hack since virtual member call in constructor of RouterImpl

    @Inject
    public CustomUpnpRouter(UpnpServiceConfiguration configuration, ProtocolFactory protocolFactory, RemoteDeviceAuthorizer authorizer) {
       super(configuration, init(protocolFactory, authorizer));
    }

    public static ProtocolFactory init(ProtocolFactory protocolFactory, RemoteDeviceAuthorizer authorizer){
        if(authorizer == null) throw new ArgumentNullException("authorizer");
        CustomUpnpRouter.deviceAuthorizer = authorizer;
        return protocolFactory;
    }

    @Override
    public void received(IncomingDatagramMessage msg){
        if(deviceAuthorizer.isAuthorized(msg.getSourceAddress())){
            super.received(msg);
        }
    }

    @Override
    public void received(UpnpStream stream){
       if(stream instanceof HttpExchangeUpnpStream){
           HttpExchange exchange = ((HttpExchangeUpnpStream)stream).getHttpExchange();
           if(deviceAuthorizer.isAuthorized(exchange.getRemoteAddress().getAddress())){
               super.received(stream);
           }
       }else{
           throw new IllegalStateException("Unknown UpnpStream: " + stream);
       }
    }
}
