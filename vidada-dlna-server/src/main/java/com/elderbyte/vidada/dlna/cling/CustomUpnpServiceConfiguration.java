package com.elderbyte.vidada.dlna.cling;

import com.google.api.client.http.HttpRequestFactory;
import org.fourthline.cling.DefaultUpnpServiceConfiguration;

import org.fourthline.cling.transport.impl.DatagramIOConfigurationImpl;
import org.fourthline.cling.transport.impl.DatagramIOImpl;
import org.fourthline.cling.transport.impl.StreamServerConfigurationImpl;
import org.fourthline.cling.transport.impl.StreamServerImpl;
import org.fourthline.cling.transport.impl.jetty.StreamClientConfigurationImpl;
import org.fourthline.cling.transport.spi.DatagramIO;
import org.fourthline.cling.transport.spi.NetworkAddressFactory;
import org.fourthline.cling.transport.spi.StreamClient;
import org.fourthline.cling.transport.spi.StreamServer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class CustomUpnpServiceConfiguration extends DefaultUpnpServiceConfiguration {

    private ExecutorService executorService = Executors.newCachedThreadPool();
    private HttpRequestFactory requestFactory;
    private final int datagramPort;


    public CustomUpnpServiceConfiguration(HttpRequestFactory requestFactory, int streamListenPort, int datagramPort){
        super(streamListenPort);
        this.datagramPort = datagramPort;
        this.requestFactory = requestFactory;
    }

    @Override
    public StreamClient createStreamClient(){
        return new HttpStreamClient(new StreamClientConfigurationImpl(executorService), this.requestFactory);
    }

    @Override
    public DatagramIO createDatagramIO(NetworkAddressFactory networkAddressFactory) {
        return new CustomDatagramIOImpl(new DatagramIOConfigurationImpl(), datagramPort);
    }

}
