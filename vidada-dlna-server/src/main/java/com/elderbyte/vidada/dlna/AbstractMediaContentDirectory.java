package com.elderbyte.vidada.dlna;


import org.fourthline.cling.support.contentdirectory.AbstractContentDirectoryService;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryErrorCode;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryException;
import org.fourthline.cling.support.contentdirectory.DIDLParser;
import org.fourthline.cling.support.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public abstract class AbstractMediaContentDirectory extends AbstractContentDirectoryService {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @Override
    public BrowseResult browse(
        String objectID, BrowseFlag browseFlag, String search,
        long firstResult, long maxResults, SortCriterion[] orderBy) throws ContentDirectoryException {

        try {
            log.info(
                "Handling browse request: objectID: " + objectID + " browseFlag: " + browseFlag +
                    " fR: " +firstResult + " mR: " + maxResults);

            Pageable pageable = toPageRequest(firstResult, maxResults, orderBy);

            if(objectID.equals("0")){
                objectID = "/";
            }
            return handleBrowseRequest(objectID, search, pageable);

        } catch (Exception ex) {

            log.error("Error while handling request!", ex);

            throw new ContentDirectoryException(
                ContentDirectoryErrorCode.CANNOT_PROCESS,
                ex.toString()
            );
        }
    }

    /***************************************************************************
     *                                                                         *
     * Protected methods                                                       *
     *                                                                         *
     **************************************************************************/


    protected abstract BrowseResult handleBrowseRequest(String parentId, String search, Pageable pageable) throws Exception;


    protected BrowseResult buildBrowseResult(List<DIDLObject> nodes, long total) throws Exception {

        DIDLContent content = buildDlnaContent(nodes);

        return new BrowseResult(
            new DIDLParser().generate(content),
            nodes.size(),
            total
        );
    }

    /***************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/


    private DIDLContent buildDlnaContent(Iterable<DIDLObject> nodes){
        DIDLContent content = new DIDLContent();
        nodes.forEach(m -> content.addObject(m));
        return content;
    }


    private Pageable toPageRequest(long firstResult, long maxResults, SortCriterion[] orderby){
        if(maxResults <= 0){
            maxResults = 30;
        }
        int page = (int)(firstResult / maxResults);
        return PageRequest.of(page, (int)maxResults);
    }

}
