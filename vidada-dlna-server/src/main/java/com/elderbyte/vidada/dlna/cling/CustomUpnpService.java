package com.elderbyte.vidada.dlna.cling;

import com.elderbyte.vidada.dlna.RemoteDeviceAuthorizer;
import org.fourthline.cling.UpnpServiceConfiguration;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.protocol.ProtocolFactory;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;
import org.fourthline.cling.transport.Router;

public class CustomUpnpService extends UpnpServiceImpl{

    private static RemoteDeviceAuthorizer deviceAuthorizer; // Hack since virtual member call in constructor in UpnpServiceImpl

    public CustomUpnpService(UpnpServiceConfiguration configuration, RemoteDeviceAuthorizer deviceAuthorizer, RegistryListener... registryListeners){
        super(init(configuration, deviceAuthorizer), registryListeners);
    }

    protected Router createRouter(ProtocolFactory protocolFactory, Registry registry) {
        return new CustomUpnpRouter(getConfiguration(), protocolFactory, deviceAuthorizer);
    }

    private static UpnpServiceConfiguration init(UpnpServiceConfiguration configuration, RemoteDeviceAuthorizer deviceAuthorizer){
        CustomUpnpService.deviceAuthorizer = deviceAuthorizer;
        return configuration;
    }
}
