package com.elderbyte.vidada.dlna;

import com.elderbyte.vidada.VidadaDlnaSettings;
import com.elderbyte.vidada.dlna.cling.CustomUpnpService;
import com.elderbyte.vidada.dlna.cling.CustomUpnpServiceConfiguration;
import com.google.api.client.http.HttpRequestFactory;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.meta.LocalDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.PreDestroy;

/**
 * Manages the embedded DLNA-Server
 */
@Service
public class DlnaService {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HttpRequestFactory requestFactory;

    @Autowired
    private VidadaDlnaSettings settings;

    private UpnpService dlnaService = null;
    private Thread dlnaThread;
    private final Object dlnaThreadLock = new Object();


    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/


    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    /**
     * Starts the DLNA Server
     */
    public void startDlnaServerAsync(){
        synchronized (dlnaThreadLock){
            if(dlnaService == null){

                log.info("Starting DLNA server ...");

                // Start a user thread that runs the UPnP stack
                dlnaThread = new Thread(() -> {
                    dlnaService = startDlnaServer();
                });
                dlnaThread.setDaemon(false);
                dlnaThread.start();
            }else{
                log.warn("DLNA Server already running, cant start again!");
            }
        }
    }

    /**
     * Stops the DLNA Server if it is currently running
     */
    @PreDestroy
    public void stopDlnaServer(){
        synchronized (dlnaThreadLock){

            if(dlnaThread != null){
                log.info("Stopping DLNA server ...");
                final UpnpService dlnaService = this.dlnaService;
                if(dlnaService != null){
                    dlnaService.shutdown();
                    this.dlnaService = null;
                    this.dlnaThread = null;
                }
            }else{
                log.warn("DLNA Server already stopped, cant stop again!");
            }
        }
    }

    /**
     * Checks if the DLNA Server is currently running
     */
    public boolean isDlnaServerRunning(){
        return (this.dlnaThread != null);
    }

    /**************************************************************************
     *                                                                         *
     * Private Methods                                                         *
     *                                                                         *
     **************************************************************************/

    /**
     * Starts a Dlna Server using the current configuration.
     */
    private UpnpService startDlnaServer(){
        try {

            String serverName = settings.getServerName();


            final UpnpService upnpService = new CustomUpnpService(
                new CustomUpnpServiceConfiguration(
                    this.requestFactory,
                    settings.getServerDlnaPort(),
                    1901),
                RemoteDeviceAuthorizer.ALLOW_EVERYONE
            );

            LocalDevice mediaServer = DlnaMediaServiceBuilder
                .create(serverName)
                .description(serverName + " MediaServer")
                .build();

            // Add the bound local device to the registry
            upnpService.getRegistry().addDevice(mediaServer);

            log.info("DLNAServer started successfully.");

            return upnpService;
        } catch (Exception ex) {
            throw new RuntimeException("DLNA server encountered an exception.", ex);
        }
    }


}
