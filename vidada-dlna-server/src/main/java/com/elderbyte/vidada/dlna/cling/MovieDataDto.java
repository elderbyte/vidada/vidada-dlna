package com.elderbyte.vidada.dlna.cling;

public class MovieDataDto {

    public String title;
    public String streamUrl;
    public String mimeType;
    public long size;
    public String duration;
    public long bitrate;


    public MovieDataDto(){}

    public MovieDataDto(String title, String streamUrl, String mimeType, long size, String duration, long bitrate) {
        this.title = title;
        this.streamUrl = streamUrl;
        this.mimeType = mimeType;
        this.size = size;
        this.duration = duration;
        this.bitrate = bitrate;
    }
}
