package com.elderbyte.vidada.dlna;


import com.elderbyte.vidada.browser.MediaBrowserService;
import com.elderbyte.vidada.client.documents.BrowseNodeDto;
import com.elderbyte.vidada.dlna.cling.DidlObjectBuilder;
import com.elderbyte.vidada.dlna.cling.SpringApplicationContext;
import org.fourthline.cling.support.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;


public class DlnaMediaContentDirectory extends AbstractMediaContentDirectory {


    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final MediaBrowserService mediaBrowserClient;

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/


    public DlnaMediaContentDirectory(){
        this.mediaBrowserClient = SpringApplicationContext.getBean(MediaBrowserService.class);
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    protected BrowseResult handleBrowseRequest(String parentId, String search, Pageable pageable) throws Exception {

        var result = mediaBrowserClient.listNodes(parentId, "", "", pageable);

        var total = result.getTotalPages();
        log.info("BrowseNode result page: " + total + " @ " + pageable);

        return buildBrowseResult(parentId, result.getContent(), total);
    }

    /***************************************************************************
     *                                                                         *
     * Private methods                                                         *
     *                                                                         *
     **************************************************************************/

    private BrowseResult buildBrowseResult(String parentId, List<BrowseNodeDto> result, long total) throws Exception {
        List<DIDLObject> nodes = result.stream()
            .map(m -> DidlObjectBuilder.buildDlnaItem(m, parentId))
            .filter(Optional::isPresent).map(Optional::get)
            .collect(toList());

        return buildBrowseResult(nodes, total);
    }

}
