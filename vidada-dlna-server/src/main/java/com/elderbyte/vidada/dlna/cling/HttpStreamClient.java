package com.elderbyte.vidada.dlna.cling;

import com.google.api.client.http.*;
import org.apache.commons.io.IOUtils;
import org.fourthline.cling.model.message.*;
import org.fourthline.cling.model.message.header.ContentTypeHeader;
import org.fourthline.cling.transport.spi.StreamClient;
import org.fourthline.cling.transport.spi.StreamClientConfiguration;
import org.seamless.util.MimeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

public class HttpStreamClient implements StreamClient {


    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final StreamClientConfiguration configuration;
    private HttpRequestFactory requestFactory;

    public HttpStreamClient(StreamClientConfiguration configuration, HttpRequestFactory requestFactory){
        this.configuration = configuration;
        this.requestFactory = requestFactory;
    }

    @Override
    public StreamResponseMessage sendRequest(StreamRequestMessage message) throws InterruptedException {

        try {
            HttpRequest request = buildRequest(message);

            HttpResponse response = request.execute();

            return convertResponse(response);

        } catch (IOException e) {
            throw new RuntimeException("Request failed!", e);
        }
    }

    private HttpRequest buildRequest(StreamRequestMessage message) throws IOException {

        HttpRequest request = requestFactory.buildRequest(
            message.getOperation().getMethod().getHttpName(),
            new GenericUrl(message.getUri()),
            null
        );

        applyBody(message, request);

        applyHeaders(message, request);

        return request;

    }


    private void applyHeaders(StreamRequestMessage message, HttpRequest request){
        UpnpHeaders headers = message.getHeaders();

        // Fill in headers
        for(Map.Entry<String,List<String>> entry :  headers.entrySet()){
            request.getHeaders().set(entry.getKey(), entry.getValue());
        }
    }

    private void applyBody(StreamRequestMessage message, HttpRequest request) throws IOException {

        if (message.hasBody()) {
            byte[] buffer;
            MimeType contentType;

            if (message.getBodyType() == UpnpMessage.BodyType.STRING) {
                contentType =
                    message.getContentTypeHeader() != null
                        ? message.getContentTypeHeader().getValue()
                        : ContentTypeHeader.DEFAULT_CONTENT_TYPE_UTF8;

                String charset =
                    message.getContentTypeCharset() != null
                        ? message.getContentTypeCharset()
                        : "UTF-8";

                try {
                    buffer = message.getBodyString().getBytes(charset);
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException("Unsupported character encoding: " + charset, ex);
                }
            } else {
                if (message.getContentTypeHeader() == null)
                    throw new RuntimeException(
                        "Missing content type header in request message: " + message
                    );
                contentType = message.getContentTypeHeader().getValue();
                buffer = message.getBodyBytes();
            }
            HttpContent content = new ByteArrayContent(contentType.toString(), buffer);
            request.setContent(content);
        }
    }



    private StreamResponseMessage convertResponse(HttpResponse response) throws IOException {

        // Status
        UpnpResponse responseOperation =
            new UpnpResponse(
                response.getStatusCode(),
                UpnpResponse.Status.getByStatusCode(response.getStatusCode()).getStatusMsg()
            );

        //log.fine("Received response: " + responseOperation);

        StreamResponseMessage responseMessage = new StreamResponseMessage(responseOperation);

        // Headers
        UpnpHeaders headers = new UpnpHeaders();

        for(Map.Entry<String, Object> entry : response.getHeaders().entrySet()){
            for(String value : response.getHeaders().getHeaderStringValues(entry.getKey())){
                headers.add(entry.getKey(), value);
            }
        }
        responseMessage.setHeaders(headers);

        // Body
        byte[] bytes = IOUtils.toByteArray(response.getContent());

        if (bytes != null && bytes.length > 0 && responseMessage.isContentTypeMissingOrText()) {
            try {
                responseMessage.setBodyCharacters(bytes);
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException("Unsupported character encoding: " + ex, ex);
            }
        } else if (bytes != null && bytes.length > 0) {
            responseMessage.setBody(UpnpMessage.BodyType.BYTES, bytes);
        } else {
            // No body
        }
        return responseMessage;
    }


    @Override
    public void stop() {
        // NOP
    }

    @Override
    public StreamClientConfiguration getConfiguration() {
        return this.configuration;
    }
}
