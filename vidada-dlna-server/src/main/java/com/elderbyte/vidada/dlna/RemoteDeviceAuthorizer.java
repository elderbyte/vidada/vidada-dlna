package com.elderbyte.vidada.dlna;

import java.net.InetAddress;


public interface RemoteDeviceAuthorizer {

    /**
     * Grants access to all devices
     */
    RemoteDeviceAuthorizer ALLOW_EVERYONE = (address) -> true;

    /**
     * Checks if the given device (IP) is granted access.
     */
    boolean isAuthorized(InetAddress address);
}
