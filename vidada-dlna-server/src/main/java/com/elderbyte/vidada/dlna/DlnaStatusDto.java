package com.elderbyte.vidada.dlna;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(
    fieldVisibility = JsonAutoDetect.Visibility.ANY,
    isGetterVisibility = JsonAutoDetect.Visibility.NONE,
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    setterVisibility = JsonAutoDetect.Visibility.NONE)
public class DlnaStatusDto {

    public boolean enabled;

    public DlnaStatusDto(){ }

    public DlnaStatusDto(boolean enabled){
        this.enabled = enabled;
    }


    @Override
    public String toString() {
        return "DlnaStatusDto{" +
            "enabled=" + enabled +
            '}';
    }
}
