package com.elderbyte.vidada.browser;

import com.elderbyte.vidada.client.documents.BrowseNodeDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MediaBrowserService {

    Page<BrowseNodeDto> listNodes(String vpath,
                                                String queryStr,
                                                String tagExpression,
                                                Pageable pageable);
}
