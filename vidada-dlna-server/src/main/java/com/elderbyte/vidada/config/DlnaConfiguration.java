package com.elderbyte.vidada.config;

import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.ApacheHttpTransport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DlnaConfiguration {

    @Bean
    public HttpRequestFactory requestFactory() {
        final HttpTransport HTTP_TRANSPORT = new ApacheHttpTransport();
        return  HTTP_TRANSPORT.createRequestFactory();
    }

}
