package com.elderbyte.vidada.config;

/**
 * Application constants.
 */
public final class Constants {

    private Constants() {
    }

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";



    public static final String SPRING_PROFILE_FAST = "fast";


}
