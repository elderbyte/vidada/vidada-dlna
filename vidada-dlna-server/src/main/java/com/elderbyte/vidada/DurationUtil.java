package com.elderbyte.vidada;

public class DurationUtil {

    public static String formatDuration(int seconds){
        return String.format("%d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60));
    }

}
