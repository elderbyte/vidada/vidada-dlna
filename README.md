[![Build Status](https://travis-ci.org/Vidada-Project/vidada-dlna.svg?branch=master)](https://travis-ci.org/Vidada-Project/vidada-dlna)
[![Docker Pulls](https://img.shields.io/docker/pulls/elderbyte/vidada-dlna.svg)](https://hub.docker.com/r/elderbyte/vidada-dlna/)



# vidada-dlna
Support exposing &amp; browsing a [vidada server](https://github.com/Vidada-Project/vidada-server) over dlna. 


# Configuration & Env Variables

You can configure the most important aspects using ENV variables. They are valid using the jar directly or the docker container.

| Env               |      Description      |  Hint      |
|-------------------|:---------------------:|-----------:|
| PORT              |  Url of Eureka server | 80 |
| DLNA_PORT              |  Dlna port | 32470 |
| DLNA_MULTICAST_PORT              |  Dlna port | 1901 |
| DLNA_SERVER_NAME              |  The display name of the server | VidadaDlna |
| VIDADA_SERVER_URL              |  The url to a vidada server | localhost:80 |
| VIDADA_USER              |  The user name | (optional) |
| VIDADA_KEY              |  The key | (optional) |
